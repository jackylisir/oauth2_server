package com.example.demo.controllers

import com.example.demo.model.Demo
import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class DemoController {

    @Value("\${cfg.group.a}")
    lateinit var a : String
    @RequestMapping("/public/index")
    fun index(): Demo {
        logger.info("hello index")
        return Demo("demo", 10, a)
    }

    @RequestMapping("/demo/index")
    fun hello(): Demo {
        logger.info("hello api")
        return Demo("hello", 10, "hello desc")
    }

    @RequestMapping("/demo/index1")
    fun hello1(): Demo {
        logger.info("hello api")
        return Demo("hello", 10, "hello desc")
    }
    companion object : KLogging()


}